import java.util.Iterator;
import java.util.Random;
import java.util.TreeSet;
import java.util.Set;

public class PerceptronExperiment extends SupervisedLearner {

    private Perceptron[] perceptrons;
    private Random rand;

    public PerceptronExperiment(Random rand, boolean verbose) {
        super(verbose);
        this.perceptrons = null;
        this.rand = rand;
    }

    @Override
    public void train(Matrix features, Matrix labels) throws Exception {
        Matrix tempLabels;

        // generate perceptrons, one for every output class/label
        Set classes = new TreeSet();
        for(int i = 0; i < labels.rows(); i++) {
            classes.add(labels.get(i, 0));
        }

        // train each perceptron
        perceptrons = new Perceptron[classes.size()];
        Iterator iter = classes.iterator();
        double outputClass;

        while(iter.hasNext()) {
            outputClass = (double) iter.next();
            tempLabels = labels;

            // modfiy output labels to match perceptron output
            for(int i = 0; i < labels.rows(); i++) {
                if(labels.get(i, 0) == outputClass) {
                    labels.set(i, 0, 1);
                }
                else {
                    labels.set(i, 0, 0);
                }
            }
            perceptrons[(int) outputClass] = new Perceptron(this.rand, this.getVerbose());
            perceptrons[(int) outputClass].train(features, tempLabels);
        }
    }

    @Override
    public void predict(double[] features, double[] labels) throws Exception {
        // run each perceptron, collect net values
        // depending on which perceptron is positive and has the largest net value, use that output class
    }
}
