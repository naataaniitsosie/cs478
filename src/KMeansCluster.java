import java.util.ArrayList;
import java.util.Collections;

public class KMeansCluster extends UnsupervisedLearner {

    /**
     * Number of clusters
     */
    public static final int k = 7;

    /**
     * centroids
     */
    ArrayList<double[]> centroids;

    /**
     * sum squared errors of the clusters
     */
    ArrayList<Double> clusterSse;

    /**
     * clusters, index of instances that belong to each cluster
     */
    ArrayList<ArrayList<Double>> clusters;

    /**
     * the average silhouette score of each cluster
     */
    ArrayList<Double> avgSilhouette;

    public KMeansCluster(boolean verbose, Matrix features) {
        super(verbose, features);

        /* Initialization */

        // assign the first k instances to be initial centroids
        this.centroids = new ArrayList<>();
        this.clusters = new ArrayList<>();
        this.clusterSse = new ArrayList<>();
        this.avgSilhouette = new ArrayList<>();
        for(int i = 0; i < k; i++) {
            this.centroids.add(features.row(i));
            this.clusters.add(new ArrayList<>());
            this.clusterSse.add(0.0);
            this.avgSilhouette.add(0.0);
        }
    }

    @Override
    public void learn() throws Exception {

        /* Learning */

        // variables
        ArrayList<Double> cluster;
        boolean changeInSse = true;
        int clusterIndex;
        int iteration = 0;
        double error;
        double totalSse;
        double prevSse = -1;
        double avgSil;
        double totalSil = 0;
        double[] centroid;
        double[] output = new double[2];

        while(changeInSse) {
            iteration++;

            // reset sse and silhouette
            totalSse = 0;
            for (int n = 0; n < this.clusterSse.size(); n++) {
                this.clusterSse.set(n, 0.0);
            }

            // clear clusters
            for(int r = 0; r < this.clusters.size(); r++) {
                this.clusters.get(r).clear();
            }

            // for each datapoint
            for (int i = 0; i < this.features.rows(); i++) {

                // partition into a cluster
                this.predict(this.features.row(i), output);
                clusterIndex = (int) output[0];
                this.clusters.get(clusterIndex).add((double) i);

                // update sse
                error = this.clusterSse.get(clusterIndex) + output[1];
                this.clusterSse.set(clusterIndex, error);
            }

            // calculate new centroids
            for (int j = 0; j < this.clusters.size(); j++) {
                centroid = this.centroid(this.clusters.get(j));
                this.centroids.set(j, centroid);
            }

            // calculate average silhouette distances of clusters
            for (int j = 0; j < this.k; j++) {
                cluster = this.clusters.get(j);
                avgSil = 0;
                for(int d = 0; d < cluster.size(); d++) {
                    avgSil += this.silhouette(this.features.row(cluster.get(d).intValue()), j) / cluster.size();
                }
                this.avgSilhouette.set(j, avgSil);
            }

            // calculate sse across all clusters
            for (int m = 0; m < this.clusterSse.size(); m++) {
                totalSse += this.clusterSse.get(m);
            }

            if(prevSse == totalSse) {
                changeInSse = false;
            }
            prevSse = totalSse;
        }

        // calculate silhouette across all clusters
        for (int m = 0; m < this.clusterSse.size(); m++) {
            totalSil += this.avgSilhouette.get(m);
        }
        totalSil /= this.k;


        System.out.println("k = " + this.k);
        System.out.println("Clusters:");
        for(int i = 0; i < this.k; i++) {
            System.out.println(i + ")");
            System.out.println("Centroid: " + this.printInstance(this.centroids.get(i)));
            System.out.println("Size: " + this.clusters.get(i).size());
            System.out.println("SSE: " + this.clusterSse.get(i));
            System.out.println("Avg. Silhouette: " + this.avgSilhouette.get(i));

        }
        System.out.println();
        System.out.println("SSE of all clusters: " + prevSse);
        System.out.println("Avg. Silhouette of all clusters: " + totalSil);
    }

    /**
     *
     * @param instance
     * @param output returns the cluster index and sse
     * @return
     * @throws Exception
     */
    @Override
    public void predict(double[] instance, double[] output) throws Exception {

        /* Usage */

        // variables
        double[] distances = new double[k];
        int minIndex = 0;

        // compute the distance to each cluster centre
        for(int i = 0; i < k; i++) {
            distances[i] = distance(instance, this.centroids.get(i));
        }

        // assign the datapoint to the nearest cluster centre
        for(int j = 0; j < k; j++) {
            if(distances[j] < distances[minIndex]) {
                minIndex = j;
            }
        }

        output[0] = minIndex;
        output[1] = Math.pow(distances[minIndex], 2);
    }

    /**
     * calculates centroid of cluster
     */
    private double[] centroid(ArrayList<Double> cluster) {

        // variables
        int numAttr = this.features.cols();
        int size = cluster.size();
        double[] centroid = new double[numAttr];
        Matrix mCluster = new Matrix(this.features, 0, 0, 0, numAttr);

        // load cluster into Matrix
        for(int n = 0; n < size; n++) {
            mCluster.m_data.add(this.features.row(cluster.get(n).intValue()));
        }


        // calculate centroid
        for(int c = 0; c < numAttr; c++) {

            // if continuous
            if(mCluster.valueCount(c) == 0) {
                centroid[c] = mCluster.columnMean(c);
            }

            // else nominal
            else {
                centroid[c] = mCluster.mostCommonValue(c);
            }
            if(centroid[c] == Double.NaN) {
                centroid[c] = Matrix.UNKNOWN;
            }
        }
        return centroid;
    }

    /**
     * calculates the distance between a two points
     */
    private double distance(double[] point, double[] point2) {
        // Euclidian distance
        double distance = 0.0;
        double a;
        double b;
        double d;

        for (int c = 0; c < this.features.cols(); c++) {
            a = point[c];
            b = point2[c];

            // if continuous
            if(this.features.valueCount(c) == 0) {
                if(a == Matrix.UNKNOWN || b == Matrix.UNKNOWN) {
                    d = 1.0;
                }
                else {
                    d = b - a;
                }
            }

            // else nominal
            else {
                if(a == Matrix.UNKNOWN || b == Matrix.UNKNOWN) {
                    d = 1.0;
                }
                else if(a == b) {
                    d = 0.0;
                }
                else {
                    d = 1.0;
                }
            }

            distance += Math.pow(d, 2);
        }
        return Math.sqrt(distance);
    }

    private double silhouette(double[] instance, int clusterIndex) {

        // variables
        double s = 0;
        double a;
        double b;
        double[] datum;
        double dissimilarity;
        double sumDistance;
        ArrayList<Double> bValues = new ArrayList<>();
        ArrayList<Double> cluster;

        // for each cluster
        for(int i = 0; i < k; i++) {
            bValues.add(0.0);

            // calculate b values
            cluster = this.clusters.get(i);
            for(int d = 0; d < cluster.size(); d++) {
                datum = this.features.row(cluster.get(d).intValue());
                bValues.set(i, bValues.get(i) + this.distance(instance, datum));
            }
        }
        a = bValues.get(clusterIndex);
        bValues.remove(clusterIndex);
        b = Collections.min(bValues);

        // calculate silhouette
        if(a < b) {
            s = 1.0 - a/b;
        }
        else if(a == b) {
            s = 0;
        }
        else {
            s = b/a - 1;
        }

        return s;

    }

    private String printInstance(double[] instance) {
        String s = "";
        int numAttr = this.features.cols();
        for(int i = 0; i < numAttr; i++) {

            // if continuous, print value
            if(this.features.valueCount(i) == 0) {
                s += String.format("%.3f", instance[i]);
            }
            // else nominal
            else {
                s += this.features.m_enum_to_str.get(i).get((int) instance[i]);
            }

            if(i + 1 < numAttr)
            s += ", ";
        }
        return s;
    }

}
