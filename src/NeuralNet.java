import java.util.Random;

public class NeuralNet extends SupervisedLearner {

    private static final double BIAS_INPUT = 1.0;
    private static final double BIAS_WEIGHT = 1.0;
    private static final double LEARNING_RATE = 0.1;
    private static final double MOMENTUM_TERM = 0.9;
    private static final double VALIDATION_SET_SIZE = 0.10;
    private static final int BIAS = 1;
    private static final int HIDDEN_LAYERS = 2;
    private static final int INPUT_HIDDEN_RATIO = 1;
    private static final int MAX_BSSF_COUNT = 5;

    private Random rand;
    private Node[] nodes;

    /**
     *  the start indexes of each layer in the nodes array
     *  */
    private int inputIndex;
    private int hiddenIndex;
    private int outputIndex;

    /**
     * weights, indexed source to destination node, [src][dest]
     * */
    private double[][] weights;
    private double[][] deltaWeights;

    public NeuralNet(Random rand, boolean verbose) {
        super(verbose);
        this.nodes = null;
        this.rand = rand;
        this.weights = null;
        this.inputIndex = -1;
        this.hiddenIndex = -1;
        this.outputIndex = -1;
    }

    @Override
    public void train(Matrix features, Matrix labels) throws Exception {

        // create network structure
        int numInput = features.cols();
        this.inputIndex = 0;
        int numHidden = HIDDEN_LAYERS * (INPUT_HIDDEN_RATIO * numInput + BIAS);
        this.hiddenIndex = numInput + 1;
        int numOutput = labels.valueCount(0);
        if(numOutput == 0) {
            // if continuous
            numOutput = labels.cols();
        }
        else {
            // if nominal
            numOutput = labels.valueCount(0);
        }
        this.outputIndex = this.hiddenIndex + numHidden;
        this.initNodes(numInput + BIAS, numHidden, numOutput);
        //this.initTestNodes(); // REMOVE WHEN FINISHED TESTING
        this.initWeights();
        this.initTestWeights(); // REMOVE WHEN FINISHED TESTING

        // variables
        boolean done = false;
        boolean verbose = this.getVerbose();
        double bssf = Double.NEGATIVE_INFINITY;
        double accuracy;
        double netValue;
        double netValuePrime;
        double output;
        double sse;
        double ssePrev;
        double sum;
        double target;
        double[] util;
        double[] outputVector;
        double[] targetVector;
        if(labels.cols() == 1) {
            outputVector = new double[labels.valueCount(0)];
            targetVector = new double[labels.valueCount(0)];
        }
        else {
            outputVector = new double[labels.cols()];
            targetVector = new double[labels.cols()];
        }
        int bssfCount = 0;
        int epochCount = -1;
        int numInstances;
        Matrix validationFeatures;
        Matrix validationLabels;
        Matrix[] temp = new Matrix[2];

        // create validation set
        features.shuffle(this.rand, labels);

        temp[0] = features;
        temp[1] = null;
        this.validationSet(temp);
        features = temp[0];
        validationFeatures = temp[1];

        temp[0] = labels;
        temp[1] = null;
        this.validationSet(temp);
        labels = temp[0];
        validationLabels = temp[1];

        numInstances = features.rows();

        // training algorithm
        while(!done) {
            epochCount++;
            features.shuffle(this.rand, labels);

            // iterate on instances
            for(int n = 0; n < numInstances; n++) {

                // prepare target vector
                if(labels.cols() == 1) {
                    target = labels.get(n, 0);
                    if(labels.valueCount(0) != 0) {
                        // if nominal
                        for (int x = 0; x < this.nodes.length - this.outputIndex; x++) {
                            if (x != target) {
                                targetVector[x] = 0.0;
                            } else {
                                targetVector[x] = 1.0;
                            }
                        }
                    }
                    else {
                        // if continuous
                    }
                    target = labels.row(n)[0];
                    if(targetVector.length == 1) {
                        targetVector[0] = target;
                    }

                }
                else {
                    util = labels.row(n);
                    for(int t = 0; t < targetVector.length; t++) {
                        targetVector[t] = util[t];
                    }
                }


                // forwards phase (predict)
                this.predict(features.row(n), outputVector);


                // backwards phase

                // calculate error of output nodes and changes in weights
                for(int j = this.outputIndex; j < this.nodes.length; j++) {
                    netValuePrime = this.netValuePrime(j);
                    output = this.nodes[j].output;
                    target = targetVector[j - this.outputIndex];
                    this.nodes[j].error = (target - output) * netValuePrime;

                    for(int i = this.hiddenIndex; i < this.outputIndex; i++) {
                        this.updateDeltaWeight(i, j);
                    }

                }
                // calculate error of hidden nodes and changes in weights
                for(int j = this.hiddenIndex; j < this.outputIndex; j++) {
                    sum = 0;
                    for(int k = this.outputIndex; k < this.nodes.length; k++) {
                        sum += this.nodes[k].error * this.weights[j][k];
                    }
                    this.nodes[j].error = sum * this.netValuePrime(j);
                    for(int i = this.inputIndex; i < hiddenIndex; i++) {
                        this.updateDeltaWeight(i, j);
                    }
                }

                // stocastic/on-line wieght update
                this.updateWeights();

                if(verbose) {
                    /*System.out.println("Epoch: " + epochCount);
                    System.out.print("Instance: " + n + " -- { ");
                    double[] temp = features.row(n);
                    for(int i = 0; i < temp.length; i++) {
                        System.out.print(temp[i] + " ");
                    }
                    System.out.print(labels.row(n)[0] + " ");
                    System.out.println("}");
                    System.out.println("Output: " + outputVector[0]);
                    System.out.println(this.prettyPrint());*/
                }
            }

            // check stopping criteria
            accuracy = this.measureAccuracy(validationFeatures, validationLabels, null);
            if(accuracy > bssf) {
                bssf = accuracy;
                bssfCount = 0;
            }
            else {
                bssfCount++;
                if(bssfCount >= MAX_BSSF_COUNT) {
                    done = true;
                }
            }

        }
    }

    @Override
    public void predict(double[] features, double[] labels) throws Exception {
        boolean verbose = this.getVerbose();
        double netValue;

        // input into network
        this.setInputNodes(features);

        // calculate output of hidden layer nodes
        for(int i = this.hiddenIndex; i < this.outputIndex - BIAS; i++) {
            netValue = netValue(i);
            this.nodes[i].output = this.sigmoid(netValue);
        }

        // calculate output of output layer nodes
        for(int i = this.outputIndex; i < this.nodes.length; i++) {
            netValue = netValue(i);
            this.nodes[i].output = sigmoid(netValue);
        }

        // choose output, VERIFY!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        int maxIndex = this.outputIndex;
        for(int i = maxIndex; i < this.nodes.length; i++) {
            if(this.nodes[i].output > this.nodes[maxIndex].output) {
                maxIndex = i;
            }
        }

        labels[0] = ((double) maxIndex) - this.outputIndex;
    }

    private void validationSet(Matrix[] arr) {
        Matrix m = arr[0];
        Matrix t;
        Matrix v;

        int vRows = (int) (m.rows() * VALIDATION_SET_SIZE);
        int vCol = m.cols();
        int tRow = m.rows() - vRows;
        int tCol = vCol;

        v = new Matrix(m, tRow , 0, vRows, vCol);
        t = new Matrix(m, 0, 0, tRow, tCol);

        arr[0] = t;
        arr[1] = v;
    }

    private double netValuePrime(int index) {
        return nodes[index].output * (1 - nodes[index].output);
    }

    private void updateWeights() {
        double length = weights.length;
        for(int x = 0; x < length; x++) {
            for(int y = 0; y < length; y++) {
                if(this.weights[x][y] != Double.POSITIVE_INFINITY) { //&& this.deltaWeights[x][y] != 0) {
                    this.weights[x][y] += this.deltaWeights[x][y];
                }
            }
        }
    }

    private double sse() {
        double sse = 0;
        for(int i = this.outputIndex; i < this.nodes.length; i++) {
            sse += Math.pow(this.nodes[i].error, 2);
        }
        return sse;
    }

    private double sigmoid(double x) {
        return 1 / (1 + Math.pow(Math.E, -1 * x));
    }

    private void updateDeltaWeight(int i, int j) {
        this.deltaWeights[i][j] = (MOMENTUM_TERM * deltaWeights[i][j]) + (LEARNING_RATE * this.nodes[j].error * this.nodes[i].output);

    }

    private double netValue(int index) {
        int size = nodes.length;
        double netValue = 0.0;
        double value;
        for(int x = 0; x < size; x++) {
            value = this.weights[x][index];
            if(value != Double.POSITIVE_INFINITY) {
                netValue += value * this.nodes[x].output;
            }
        }
        return netValue;
    }

    private void setInputNodes(double[] inputVector) {
        for(int i = 0; i < hiddenIndex - BIAS; i++) {
            this.nodes[i].output = inputVector[i];
        }
        this.nodes[this.hiddenIndex - 1].output = BIAS_INPUT;
        this.nodes[this.outputIndex - 1].output = BIAS_INPUT;
    }

    private void initWeights() {
        int size = this.nodes.length;
        this.weights = new double[size][size];

        // init all deltaWeights to 0.0
        this.deltaWeights = new double[size][size];

        // hidden layer data
        int hLayerCount = (this.outputIndex - this.hiddenIndex) / HIDDEN_LAYERS;

        double r = this.getRandomWeight();
        for(int x = 0; x < size; x++) {
            for(int y = 0; y < size; y++) {

                // weights of input nodes
                if(x < this.hiddenIndex) {

                    // connect to first (only) hidden layer
                    if(this.hiddenIndex <= y && y < this.hiddenIndex + hLayerCount - BIAS) {
                        if(x != this.hiddenIndex - 1) {
                            this.weights[x][y] = r;
                            r = this.getRandomWeight();
                        }
                        else {
                            this.weights[x][y] = BIAS_WEIGHT;
                        }
                    }
                    else this.weights[x][y] = Double.POSITIVE_INFINITY;
                }

                // weights of hidden nodes
                else if(x < this.outputIndex) {

                    // hidden to input and first layer (not possible)
                    if(y < this.hiddenIndex + hLayerCount) {
                        this.weights[x][y] = Double.POSITIVE_INFINITY;
                    }

                    // hidden to hidden
                    else if(x + hLayerCount < this.outputIndex) {
                        if((x - this.hiddenIndex) / hLayerCount < (y - this.hiddenIndex) / hLayerCount) {
                            if ((y - this.hiddenIndex) % hLayerCount != hLayerCount - BIAS) {
                                if((x - this.hiddenIndex) % hLayerCount != hLayerCount - BIAS) {
                                    this.weights[x][y] = r;
                                    r = this.getRandomWeight();
                                }
                                else {
                                    this.weights[x][y] = BIAS_WEIGHT;
                                }
                            } else {
                                while (y < size) {
                                    this.weights[x][y] = Double.POSITIVE_INFINITY;
                                    y++;
                                }
                            }
                        }
                        else {
                            this.weights[x][y] = Double.POSITIVE_INFINITY;
                        }
                    }

                    // hidden to output
                    else {
                        if(y < this.outputIndex) {
                            this.weights[x][y] = Double.POSITIVE_INFINITY;
                        }
                        else {
                            if (x != this.outputIndex - 1) {
                                this.weights[x][y] = r;
                                r = this.getRandomWeight();
                            } else this.weights[x][y] = BIAS_WEIGHT;
                        }
                    }
                }

                // weight of output nodes
                else this.weights[x][y] = Double.POSITIVE_INFINITY;
            }
        }
    }

    private void initTestWeights() {

        // input layer to hidden layer
        this.weights[0][3] = 0.2;
        this.weights[0][4] = 0.3;
        this.weights[1][3] = -0.1;
        this.weights[1][4] = -0.3;
        this.weights[2][3] = 0.1;
        this.weights[2][4] = -0.2;

        // hidden layer 1 to hidden layer 2
        this.weights[3][6] = -0.2;
        this.weights[3][7] = -0.1;
        this.weights[4][6] = -0.3;
        this.weights[4][7] = 0.3;
        this.weights[5][6] = 0.1;
        this.weights[5][7] = 0.2;

        // hidden layer 2 to output layer
        this.weights[6][9] = -0.1;
        this.weights[6][10] = -0.2;
        this.weights[7][9] = 0.3;
        this.weights[7][10] = -0.3;
        this.weights[8][9] = 0.2;
        this.weights[8][10] = 0.1;



        /*
        this.initWeights();
        this.weights[6][7] = .02;   // w_0=0.02
        this.weights[3][7] = -.01;  // w_1=-0.01
        this.weights[4][7] = .03;   // w_2=0.03
        this.weights[5][7] = .02;   // w_3=0.02
        this.weights[2][3] = -.01;  // w_4=-0.01
        this.weights[0][3] = -.03;  // w_5=-0.03
        this.weights[1][3] = .03;   // w_6=0.03
        this.weights[2][4] = .01;   // w_7=0.01
        this.weights[0][4] = .04;   // w_8=0.04
        this.weights[1][4] = -.02;  // w_9=-0.02
        this.weights[2][5] = -.02;  // w_10=-0.02
        this.weights[0][5] = .03;   // w_11=0.03
        this.weights[1][5] = .02;   // w_12=0.02
        */
    }

    private void initNodes(int input, int hidden, int output) {
        int size = input + hidden + output;
        this.nodes = new Node[size];
        for(int i = 0; i < size; i++) {
            this.nodes[i] = new Node(Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY);
        }
    }


    private void initTestNodes() {
        this.inputIndex = 0;
        this.hiddenIndex = 3;
        this.outputIndex = 7;
        this.initNodes(this.hiddenIndex,
                this.outputIndex - this.hiddenIndex,
                1);

    }

    private double getRandomWeight() {
        return this.rand.nextGaussian() / 100;
    }

    private String prettyPrint() {
        String network = "***NEURAL NETWORK STATE***\n";

        // weights
        String temp;
        network += "Weights\n[\n";
        for(int x = 0; x < weights.length; x++) {
            temp = " " + x + ") ";
            for(int y = 0; y < weights.length; y++) {
                temp += weights[x][y] + " ";
            }
            temp += "\n";
            network += temp;
        }
        network += "\n]\n";

        return network;
    }

    private class Node {
        public double error;
        public double output;
        Node(double error, double output) {
            this.error = error;
            this.output = output;
        }
    }
}
