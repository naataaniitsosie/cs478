
import java.util.*;

public class DecisionTree extends SupervisedLearner {

    private static final boolean PRUNE = false;
    private static final double UNKNOWN_VALUE = 1.7976931348623157E308;
    private static final double VALIDATION_SET_SIZE = 0.10;
    private static final String UNKNOWN_STRING = "'?'";
    private enum unknownMethod { IMPUTATION, MEAN, MODE };

    /**
     * The nodes of the tree. The root is the first node.
     */
    private ArrayList<Node> nodes;

    private Random rand;

    private unknownMethod unkownType;

    public DecisionTree(Random rand, boolean verbose) {
        super(verbose);
        this.rand = rand;
        this.nodes = null;
        this.unkownType = unknownMethod.IMPUTATION;
    }

    /**
     *  ID3
     *  */
    @Override
    public void train(Matrix features, Matrix labels) throws Exception {
        this.nodes = new ArrayList<Node>();

        // variables
        boolean verbose = this.getVerbose();
        int numAttributes = features.cols();
        int maxDepthIndex;
        double valdiationAccuracy;
        double originalAccuracy;
        Matrix validationFeatures;
        Matrix validationLabels;
        Matrix[] temp = new Matrix[2];
        Node currentNode;
        TreeMap map;

        // validation set (only if doing reduced error pruning
        if(PRUNE) {
            features.shuffle(this.rand, labels);

            temp[0] = features;
            temp[1] = null;
            this.validationSet(temp);
            features = temp[0];
            validationFeatures = temp[1];

            temp[0] = labels;
            temp[1] = null;
            this.validationSet(temp);
            labels = temp[0];
            validationLabels = temp[1];
        }

        // handle unknown data
        if(this.unkownType == unknownMethod.IMPUTATION) {
            for (int i = 0; i < numAttributes; i++) {
                map = features.m_str_to_enum.get(i);
                if (map.containsKey(UNKNOWN_STRING)) {
                    i = numAttributes;
                    continue;
                }
                map.put(UNKNOWN_STRING, map.size());

                map = features.m_enum_to_str.get(i);
                map.put(map.size(), UNKNOWN_STRING);
            }
        }

        // make tree
        this.makeTree(features, labels, -1, 0);

        // reduced error pruning
        if(PRUNE) {
            if(verbose) {
                System.out.println("\tOriginal Node Count: " + this.nodes.size());

                maxDepthIndex = 0;
                for(int j = 0; j < this.nodes.size(); j++) {
                    if(this.nodes.get(maxDepthIndex).depth < this.nodes.get(j).depth) {
                        maxDepthIndex = j;
                    }
                }
                System.out.println("\tDepth of Tree: " + this.nodes.get(maxDepthIndex).depth);
            }
            originalAccuracy = this.measureAccuracy(validationFeatures, validationLabels, null);

            // test each node
            for(int i = 0; i < this.nodes.size(); i++) {
                currentNode = this.nodes.get(i);

                // if a non-leaf node
                if(currentNode.label == null) {
                    currentNode.label = labels.mostCommonValue(0);
                    valdiationAccuracy = this.measureAccuracy(validationFeatures, validationLabels, null);
                    if(originalAccuracy < valdiationAccuracy) {
                        this.removeChildren(currentNode);
                        currentNode.children.clear();
                        currentNode.attribute = null;
                        originalAccuracy = valdiationAccuracy;
                    }
                    else {
                        currentNode.label = null;
                    }
                }
            }
            if(verbose) {
                System.out.println("\tPruned Node Count: " + this.nodes.size());

                maxDepthIndex = 0;
                for(int j = 0; j < this.nodes.size(); j++) {
                    if(this.nodes.get(maxDepthIndex).depth < this.nodes.get(j).depth) {
                        maxDepthIndex = j;
                    }
                }
                System.out.println("\tDepth of Tree: " + this.nodes.get(maxDepthIndex).depth);
            }
        }

    }

    @Override
    public void predict(double[] features, double[] labels) throws Exception {
        // variables
        boolean found = false;
        Double feature;
        double testFeature;
        Node currentNode = this.nodes.get(0);
        Iterator <Double> iter;

        // find path
        while(!found) {
            if(currentNode.label != null) {
                // found a leaf node
                found = true;
                labels[0] = currentNode.label;
            }
            else {
                // search through children
                iter = currentNode.children.keySet().iterator();
                testFeature = features[currentNode.attribute.intValue()];
                if(testFeature == UNKNOWN_VALUE) {
                    switch (this.unkownType) {
                        case IMPUTATION:
                            testFeature = currentNode.children.keySet().size() - 1;
                            break;
                        case MEAN:
                            testFeature = Math.round(currentNode.mean);
                            break;
                        case MODE:
                            testFeature = currentNode.mode;
                            break;
                    }
                }
                while (iter.hasNext()) {
                    feature = iter.next();
                    if(testFeature == feature) {
                        currentNode = currentNode.children.get(feature);
                        break;
                    }
                    else if(!iter.hasNext()) {
                        System.out.println("ERROR: instance was not found in tree " + Arrays.toString(features));
                    }
                }
            }
        }
    }

    public void makeTree(Matrix features, Matrix labels, int currentIndex, int splitCount) {

        // variables
        double[] gains;
        double maxGain;
        Double feature;
        int maxGainIndex;
        Node currentNode;
        Node childNode;
        Matrix split;
        Matrix splitLabels;
        Matrix[] splitData;

        // If all examples have the same label:
        if(hasOneLabel(labels)) {
            // return a leaf with that label
            //System.out.println("One label left: " + labels.get(0,0));
            this.nodes.get(currentIndex).label = labels.get(0, 0);
            return;
        }

        // if there are no attributes left to split:
        else if(splitCount >= features.cols()) {
            // return a leaf with the most common label
            //System.out.println("NO features left to split");
            this.nodes.get(currentIndex).label = labels.mostCommonValue(0);
            return;
        }
        // Else:
        else {

            // choose the feature  that maximises the information gain of S to be the next node using Equation (12.2)
            gains = this.infoGains(features, labels);
            maxGainIndex = 0;
            maxGain = gains[maxGainIndex];
            for(int i = 0; i < gains.length; i++) {
                if(maxGain < gains[i]) {
                    maxGainIndex = i;
                    maxGain = gains[maxGainIndex];
                }
            }

            // add a branch from the node for each possible value f in F
            if(currentIndex < 0) {
                // root node
                currentIndex = 0;
                splitCount = 0;
                this.nodes.add(new Node(
                        null, null, 0.0,
                        features.columnMean(maxGainIndex), features.mostCommonValue(maxGainIndex)
                        ));
            }
            currentNode = this.nodes.get(currentIndex);
            currentNode.attribute = (double) maxGainIndex;
            splitData = features.splitOnColFeatures(labels, maxGainIndex);
            for(int j = 0; j < splitData.length / 2; j++) {
                childNode = new Node(null, null, currentNode.depth + 1,
                        features.columnMean(maxGainIndex), features.mostCommonValue(maxGainIndex)
                        );
                split = splitData[j * 2];
                splitLabels = splitData[j * 2 + 1];

                // check if split has any data
                feature = new Double(j);
                if(split.rows() < 1) {
                    childNode.label = labels.mostCommonValue(0);
                    currentNode.children.put(feature, childNode);
                    this.nodes.add(childNode);
                }
                else {
                    currentNode.children.put(feature, childNode);
                    this.nodes.add(childNode);
                    makeTree(split, splitLabels,this.nodes.size() - 1, splitCount + 1);
                }
            }
        }
    }

    public double[] infoGains(Matrix features, Matrix labels) {

        // variables
        ArrayList<Double> numberList = new ArrayList<Double>();
        HashMap<Double, ArrayList<Double>> infoValues = new HashMap<Double, ArrayList<Double>>();
        double[] gains = new double[features.cols()];
        double infoS;
        double infoAttr;
        double feature;
        double numAttr = features.cols();
        double numInstances = labels.rows();
        double part;
        double temp;
        double tempInfo;
        double unknown;
        Double label;
        int attributeIndex = 0;
        HashMap<Tuple, Integer> fc;
        Iterator<Double> iter;
        Iterator<Tuple> iter2;
        Tuple tuple;

        // labelCounts maps a feature to it's frequency
        HashMap<Double, Integer> labelCounts = new HashMap<Double, Integer>();

        // labelCounts maps a feature tuple to it's frequency
        ArrayList<HashMap<Tuple, Integer>> featureCounts = new ArrayList<HashMap<Tuple, Integer>>();
        for(int j = 0; j < numAttr; j++) {
            featureCounts.add(new HashMap<Tuple, Integer>());
        }

        /* info */
        for(int i = 0; i < numInstances; i++) {

            // entire set
            label = labels.get(i, attributeIndex);
            if(labelCounts.containsKey(label)) {
                labelCounts.put(label, labelCounts.get(label) + 1);
            }
            else {
                labelCounts.put(label, 1);
            }

            // each attribute
            for(int a = 0; a < numAttr; a++) {
                fc = featureCounts.get(a);
                feature = features.get(i, a);
                if(feature == UNKNOWN_VALUE) {
                    if(this.unkownType == unknownMethod.IMPUTATION) {
                        unknown = features.m_str_to_enum.get(a).get(UNKNOWN_STRING);
                    }
                    else if(this.unkownType == unknownMethod.MEAN) {
                        unknown = Math.round(features.columnMean(a));
                    }
                    else if(this.unkownType == unknownMethod.MODE) {
                        unknown = features.mostCommonValue(a);
                    }
                    else {
                        unknown = features.m_str_to_enum.get(a).get(UNKNOWN_STRING);
                    }
                    features.set(i, a, unknown);
                }
                tuple = new Tuple(feature, label);
                if (fc.containsKey(tuple)) {
                    fc.put(tuple, fc.get(tuple) + 1);
                } else {
                    fc.put(tuple, 1);
                }
            }
        }

        // info of dataset
        infoS = 0.0;
        iter = labelCounts.keySet().iterator();
        while(iter.hasNext()) {
            label = iter.next();
            infoS += this.entropy((double) labelCounts.get(label), numInstances);
        }

        // gain of each attribute
        for(int w = 0; w < numAttr; w++) {
            infoValues.clear();
            infoAttr = 0;

            // map attribute partial info values
            fc = featureCounts.get(w);
            iter2 = fc.keySet().iterator();
            while(iter2.hasNext()) {
                tuple = iter2.next();
                if(infoValues.containsKey(tuple.x)) {
                    numberList = infoValues.get(tuple.x);
                    numberList.add((double) fc.get(tuple));
                }
                else {
                    numberList = new ArrayList<Double>();
                    numberList.add((double) fc.get(tuple));
                    infoValues.put(tuple.x, numberList);
                }
            }

            // sum values
            iter = infoValues.keySet().iterator();
            while(iter.hasNext()) {
                temp = iter.next();

                part = 0;
                tempInfo = 0;
                numberList = infoValues.get(temp);
                for(int k = 0; k < numberList.size(); k++) {
                    part += numberList.get(k);
                }
                for(int l = 0; l < numberList.size(); l++) {
                    tempInfo += this.entropy(numberList.get(l), part);
                }
                tempInfo *= (part / numInstances);
                infoAttr += tempInfo;
            }


            gains[w] = infoS - infoAttr;
        }

        return gains;
    }

    private boolean hasOneLabel(Matrix labels) {
        boolean one = true;
        double label;
        if(labels.rows() < 1) {
            one = false;
        }
        else {
            label = labels.get(0,0);
            for(int i = 1; i < labels.rows(); i++) {
                if(label != labels.get(i, 0)) {
                    one = false;
                }
            }
        }
        return one;
    }

    public double infoAttribute(Matrix features, String attribute) {
        return 0;
    }

    public double infoDataSet(Matrix s) {
        ArrayList<Double> data = new ArrayList<Double>();
        double value;

        // all possible feature data
        for(int i = 0; i < s.rows(); i++) {
            value = s.get(i, 0);
            if(!data.contains(value)) {
                data.add(value);
            }
        }
        return 0;
    }

    public double entropy(double part, double whole) {
        double entropy = 0;
        double p = part / whole;
        if(p != 0) {
            entropy = (-p) * (Math.log(p) / Math.log(2));
        }
        return entropy;
    }

    private void validationSet(Matrix[] arr) {
        Matrix m = arr[0];
        Matrix t;
        Matrix v;

        int vRows = (int) (m.rows() * VALIDATION_SET_SIZE);
        int vCol = m.cols();
        int tRow = m.rows() - vRows;
        int tCol = vCol;

        v = new Matrix(m, tRow , 0, vRows, vCol);
        t = new Matrix(m, 0, 0, tRow, tCol);

        arr[0] = t;
        arr[1] = v;
    }

    private void removeChildren(Node currentNode) {
        Node childNode;
        int numChildren = currentNode.children.size();
        Iterator<Node> iter;

        if(numChildren < 1) {
            return;
        }

        iter = currentNode.children.values().iterator();
        while(iter.hasNext()) {
            childNode = iter.next();
            removeChildren(childNode);
            this.nodes.remove(childNode);
        }
    }

    /**
     * Node class. Root node has no "data == null". Leaf as no "arrtibute == null".
     */
    private class Node {
        /**
         * The attribute being split on
         * */
        public Double attribute;

        /**
         * Child nodes. Maps a feature data to child node.
         */
        public HashMap<Double, Node> children;

        /**
         * the label if this node is reached
         */
        public Double label;

        /**
         * the depth of the node in the tree
         */
        public Double depth;

        /**
         * the average label of the node's dataset
         */
        public Double mean;

        /**
         * the most median label of the node dataset
         */
        public Double mode;

        public Node(Double attribute, Double label, Double depth, Double mean, Double mode) {
            this.attribute = attribute;
            this.children = new HashMap<Double, Node>();
            this.depth = depth;
            this.label = label;
            this.mean = mean;
            this.mode = mode;
        }
    }

    public class Tuple<X, Y> {
        public double x;
        public double y;
        public Tuple(double x, double y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public boolean equals(Object o) {
            return this.x == ((Tuple) o).x && this.y == ((Tuple) o).y;
        }

        @Override
        public int hashCode() {
            return (int) this.x * 1 + (int) this.y * 1000;
        }

    }
}