public abstract class UnsupervisedLearner {
    protected boolean verbose;
    protected Matrix features;

    protected UnsupervisedLearner(boolean verbose, Matrix features) {
        this.verbose = verbose;
        this.features = features;
    }

    public abstract void learn() throws Exception;

    public abstract void predict(double[] instance, double[] output) throws Exception;
}
