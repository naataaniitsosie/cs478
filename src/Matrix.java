// ----------------------------------------------------------------
// The contents of this file are distributed under the CC0 license.
// See http://creativecommons.org/publicdomain/zero/1.0/
// ----------------------------------------------------------------

import java.util.ArrayList;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Iterator;
import java.io.File;
import java.io.FileNotFoundException;
import java.lang.Exception;

public class Matrix {
	// Data
	ArrayList< double[] > m_data;

	// Meta-data
	ArrayList< String > m_attr_name;
	ArrayList< TreeMap<String, Integer> > m_str_to_enum;
	ArrayList< TreeMap<Integer, String> > m_enum_to_str;

	static double MISSING = Double.MAX_VALUE; // representation of missing values in the dataset
	static double UNKNOWN = 1.7976931348623157E308; // representation of unknown values in the dataset

	// Creates a 0x0 matrix. You should call loadARFF or setSize next.
	public Matrix() {}

	// Copies the specified portion of that matrix into this matrix
	public Matrix(Matrix that, int rowStart, int colStart, int rowCount, int colCount) {
		m_data = new ArrayList< double[] >();
		for(int j = 0; j < rowCount; j++) {
			double[] rowSrc = that.row(rowStart + j);
			double[] rowDest = new double[colCount];
			for(int i = 0; i < colCount; i++)
				rowDest[i] = rowSrc[colStart + i];
			m_data.add(rowDest);
		}
		m_attr_name = new ArrayList<String>();
		m_str_to_enum = new ArrayList< TreeMap<String, Integer> >();
		m_enum_to_str = new ArrayList< TreeMap<Integer, String> >();
		for(int i = 0; i < colCount; i++) {
			m_attr_name.add(that.attrName(colStart + i));
			m_str_to_enum.add(that.m_str_to_enum.get(colStart + i));
			m_enum_to_str.add(that.m_enum_to_str.get(colStart + i));
		}
	}

	// Adds a copy of the specified portion of that matrix to this matrix
	public void add(Matrix that, int rowStart, int colStart, int rowCount) throws Exception {
		if(colStart + cols() > that.cols())
			throw new Exception("out of range");
		for(int i = 0; i < cols(); i++) {
			if(that.valueCount(colStart + i) != valueCount(i))
				throw new Exception("incompatible relations");
		}
		for(int j = 0; j < rowCount; j++) {
			double[] rowSrc = that.row(rowStart + j);
			double[] rowDest = new double[cols()];
			for(int i = 0; i < cols(); i++)
				rowDest[i] = rowSrc[colStart + i];
			m_data.add(rowDest);
		}
	}

	// Splits data into separate Matrix's based on the features of a specified col
	// returns for each unique feature, new feature matrix and label matrix, ex. [split1, label1, split2, label2... splitn, labeln]
	public Matrix[] splitOnColFeatures(Matrix labels, int col) {
		int numSplits = this.m_str_to_enum.get(col).size();
		int[] indexArr = new int[numSplits];
		int featureInt;
		double[] counts = new double[numSplits];
		double[] instance;
		double feature;
		Matrix[] mArr = new Matrix[numSplits * 2];

		// calculate sizes of Matrix's
		for(int i = 0; i < this.rows(); i++) {
			feature = this.get(i, col);
			counts[(int) feature] += 1;
		}

		// split the Matrix
		for(int j = 0; j < mArr.length; j++) {
			mArr[j++] = new Matrix(this, 0, 0, (int) counts[j / 2], this.cols());
			mArr[j] = new Matrix(labels, 0, 0, (int) counts[j / 2], labels.cols());
		}
		for(int n = 0; n < this.rows(); n++) {
			instance = this.row(n);
			featureInt = (int) instance[col];
			mArr[featureInt * 2].m_data.set(indexArr[featureInt], instance);
			mArr[featureInt * 2 + 1].set(indexArr[featureInt]++, 0, labels.get(n, 0));
		}


		return mArr;
	}

	public void removeAttributes(String[] attributes) {
		int[] indexes = new int[attributes.length];
		double[] in1;
		double[] in2;
		int x;
		for(int i = 0; i < indexes.length; i++) {
			x = m_attr_name.indexOf(attributes[i]);
			for(int r = 0; r < this.rows(); r++) {
				in2 = new double[this.cols() - 1];
				in1 = this.row(r);
				for(int c1 = 0, c2 = 0; c1 < this.cols(); c1++, c2++) {
					if(c2 >= in2.length) {
						continue;
					}
					in2[c2] = in1[c1];
					if(c1 == x) {
						c2--;
					}
				}
				m_data.set(r, in2);
			}
			m_attr_name.remove(x);
			m_str_to_enum.remove(x);
			m_enum_to_str.remove(x);
		}
	}



	// Resizes this matrix (and sets all attributes to be continuous)
	public void setSize(int rows, int cols) {
		m_data = new ArrayList< double[] >();
		for(int j = 0; j < rows; j++) {
			double[] row = new double[cols];
			m_data.add(row);
		}
		m_attr_name = new ArrayList<String>();
		m_str_to_enum = new ArrayList< TreeMap<String, Integer> >();
		m_enum_to_str = new ArrayList< TreeMap<Integer, String> >();
		for(int i = 0; i < cols; i++) {
			m_attr_name.add("");
			m_str_to_enum.add(new TreeMap<String, Integer>());
			m_enum_to_str.add(new TreeMap<Integer, String>());
		}
	}

	// Loads from an ARFF file
	public void loadArff(String filename) throws Exception, FileNotFoundException {
		m_data = new ArrayList<double[]>();
		m_attr_name = new ArrayList<String>();
		m_str_to_enum = new ArrayList< TreeMap<String, Integer> >();
		m_enum_to_str = new ArrayList< TreeMap<Integer, String> >();
		boolean READDATA = false;
		Scanner s = new Scanner(new File(filename));
		while (s.hasNext()) {
			String line = s.nextLine().trim();
			if (line.length() > 0 && line.charAt(0) != '%') {
				if (!READDATA) {
					
					Scanner t = new Scanner(line);
					String firstToken = t.next().toUpperCase();
					
					if (firstToken.equals("@RELATION")) {
						String datasetName = t.nextLine();
					}
					
					if (firstToken.equals("@ATTRIBUTE")) {
						TreeMap<String, Integer> ste = new TreeMap<String, Integer>();
						m_str_to_enum.add(ste);
						TreeMap<Integer, String> ets = new TreeMap<Integer, String>();
						m_enum_to_str.add(ets);

						Scanner u = new Scanner(line);
						if (line.indexOf("'") != -1) u.useDelimiter("'");
						u.next();
						String attributeName = u.next();
						if (line.indexOf("'") != -1) attributeName = "'" + attributeName + "'";
						m_attr_name.add(attributeName);

						int vals = 0;
						String type = u.next().trim().toUpperCase();
						if (type.equals("REAL") || type.equals("CONTINUOUS") || type.equals("INTEGER")) {
						}
						else {
							try {
								String values = line.substring(line.indexOf("{")+1,line.indexOf("}"));
								Scanner v = new Scanner(values);
								v.useDelimiter(",");
								while (v.hasNext()) {
									String value = v.next().trim();
									if(value.length() > 0)
									{
										ste.put(value, new Integer(vals));
										ets.put(new Integer(vals), value);
										vals++;
									}
								}
							}
							catch (Exception e) {
								throw new Exception("Error parsing line: " + line + "\n" + e.toString());
							}
						}
					}
					if (firstToken.equals("@DATA")) {
						READDATA = true;
					}
				}
				else {
					double[] newrow = new double[cols()];
					int curPos = 0;

					try {
						Scanner t = new Scanner(line);
						t.useDelimiter(",");
						while (t.hasNext()) {
							String textValue = t.next().trim();
							//System.out.println(textValue);

							if (textValue.length() > 0) {
								double doubleValue;
								int vals = m_enum_to_str.get(curPos).size();
								
								//Missing instances appear in the dataset as a double defined as MISSING
								if (textValue.equals("?")) {
									doubleValue = MISSING;
								}
								// Continuous values appear in the instance vector as they are
								else if (vals == 0) {
									doubleValue = Double.parseDouble(textValue);
								}
								// Discrete values appear as an index to the "name" 
								// of that value in the "attributeValue" structure
								else {
									doubleValue = m_str_to_enum.get(curPos).get(textValue);
									if (doubleValue == -1) {
										throw new Exception("Error parsing the value '" + textValue + "' on line: " + line);
									}
								}
								
								newrow[curPos] = doubleValue;
								curPos++;
							}
						}
					}
					catch(Exception e) {
						throw new Exception("Error parsing line: " + line + "\n" + e.toString());
					}
					m_data.add(newrow);
				}
			}
		}
	}

	// Returns the number of rows in the matrix
	int rows() { return m_data.size(); }

	// Returns the number of columns (or attributes) in the matrix
	int cols() { return m_attr_name.size(); }

	// Returns the specified row
	double[] row(int r) { return m_data.get(r); }

	// Returns the element at the specified row and column
	double get(int r, int c) { return m_data.get(r)[c]; }

	// Sets the value at the specified row and column
	void set(int r, int c, double v) { row(r)[c] = v; }

	// Returns the name of the specified attribute
	String attrName(int col) { return m_attr_name.get(col); }

	// Set the name of the specified attribute
	void setAttrName(int col, String name) { m_attr_name.set(col, name); }

	// Returns the name of the specified value
	String attrValue(int attr, int val) { return m_enum_to_str.get(attr).get(val); }

	// Returns the number of values associated with the specified attribute (or column)
	// 0=continuous, 2=binary, 3=trinary, etc.
	int valueCount(int col) { return m_enum_to_str.get(col).size(); }

	// Shuffles the row order
	void shuffle(Random rand) {
		for(int n = rows(); n > 0; n--) {
			int i = rand.nextInt(n);
			double[] tmp = row(n - 1);
			m_data.set(n - 1, row(i));
			m_data.set(i, tmp);
		}
	}

	// Shuffles the row order with a buddy matrix 
	void shuffle(Random rand, Matrix buddy) {
		for (int n = rows(); n > 0; n--) {
			int i = rand.nextInt(n);
			double[] tmp = row(n - 1);
			m_data.set(n - 1, row(i));
			m_data.set(i, tmp);


			double[] tmp1 = buddy.row(n - 1);
			buddy.m_data.set(n - 1, buddy.row(i));
			buddy.m_data.set(i, tmp1);
		}
	}

	// Returns the mean of the specified column
	double columnMean(int col) {
		double sum = 0;
		int count = 0;
		for(int i = 0; i < rows(); i++) {
			double v = get(i, col);
			if(v != MISSING && v != UNKNOWN)
			{
				sum += v;
				count++;
			}
		}
		if(count == 0) {
			return UNKNOWN;
		}
		return sum / count;
	}

	// Returns the min value in the specified column
	double columnMin(int col) {
		double m = MISSING;
		for(int i = 0; i < rows(); i++) {
			double v = get(i, col);
			if(v != MISSING)
			{
				if(m == MISSING || v < m)
					m = v;
			}
		}
		return m;
	}

	// Returns the max value in the specified column
	double columnMax(int col) {
		double m = MISSING;
		for(int i = 0; i < rows(); i++) {
			double v = get(i, col);
			if(v != MISSING)
			{
				if(m == MISSING || v > m)
					m = v;
			}
		}
		return m;
	}

	// Returns the most common value in the specified column
	double mostCommonValue(int col) {
		TreeMap<Double, Integer> tm = new TreeMap<Double, Integer>();
		for(int i = 0; i < rows(); i++) {
			double v = get(i, col);
			if(v != MISSING)
			{
				Integer count = tm.get(v);
				if(count == null)
					tm.put(v, new Integer(1));
				else
					tm.put(v, new Integer(count.intValue() + 1));
			}
		}
		int maxCount = 0;
		double val = MISSING;
		Iterator< Entry<Double, Integer> > it = tm.entrySet().iterator();
		while(it.hasNext())
		{
			Entry<Double, Integer> e = it.next();
			if(e.getValue() > maxCount)
			{
				maxCount = e.getValue();
				val = e.getKey();
			}
		}
		return val;
	}

	/**
	 * normalizes this matrix
	 * If a partner matrix is given, this matrix is normalizes with partner
	 * @param partner
	 */
	void normalize(Matrix partner) {
		double min;
		double max;
		for(int i = 0; i < this.cols(); i++) {
			if(valueCount(i) == 0) {
				min = this.columnMin(i);
				max = this.columnMax(i);
				if(partner != null) {
					min = Math.max(min, partner.columnMin(i));
					max = Math.max(max, partner.columnMax(i));
				}
				for(int j = 0; j < this.rows(); j++) {
					double v = this.get(j, i);
					if(v != MISSING) {
						v = (v - min) / (max - min);
						this.set(j, i, v);
						if(partner != null) {
							partner.set(j, i, v);
						}
						if(v > 1) {
							System.out.println("bad normalization");
						}
					}
				}
			}
		}
	}

	boolean hasInstance(double[] instance) {
		boolean hasInstance = false;
		for(int i = 0; i < this.rows(); i++) {
			for(int j = 0; j < this.cols(); j++) {
				if(this.get(i,j) != instance[j]) {
					break;
				}

				// found instance, end loops
				hasInstance = true;
				i = this.rows();
				j = this.cols();
			}
		}
		return hasInstance;
	}


	void print() {
		System.out.println("@RELATION Untitled");
		for(int i = 0; i < m_attr_name.size(); i++) {
			System.out.print("@ATTRIBUTE " + m_attr_name.get(i));
			int vals = valueCount(i);
			if(vals == 0)
				System.out.println(" CONTINUOUS");
			else
			{
				System.out.print(" {");
				for(int j = 0; j < vals; j++) {
					if(j > 0)
						System.out.print(", ");
					System.out.print(m_enum_to_str.get(i).get(j));
				}
				System.out.println("}");
			}
		}
		System.out.println("@DATA");
		for(int i = 0; i < rows(); i++) {
			double[] r = row(i);
			for(int j = 0; j < r.length; j++) {
				if(j > 0)
					System.out.print(", ");
				if(valueCount(j) == 0)
					System.out.print(r[j]);
				else
					System.out.print(m_enum_to_str.get(j).get((int)r[j]));
			}
			System.out.println("");
		}
	}
}
