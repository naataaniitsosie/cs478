import java.util.Random;

class Perceptron extends SupervisedLearner {

    private static final double ACCURACY_CHANGE_MIN = 0.01;
    private static final double BIAS_INPUT = 1.0;
    private static final double INITIAL_WEIGHT = 0.0;
    private static final double LEARNING_RATE = .1;
    private static final double NO = 0.0;
    private static final double THRESHOLD = 0.0;
    private static final double YES = 1.0;

    private static final int DIMENSION = 1;
    private static final int INSIGNIFICANT_EPOCHS_MAX = 5;

    private Random rand;
    private double[] weights;

    public Perceptron(Random rand, boolean verbose) {
        super(verbose);
        this.rand = rand;
        this.weights = null;
    }

    @Override
    public void train(Matrix features, Matrix labels) throws Exception {
        boolean done = false;
        boolean verbose = this.getVerbose();
        double accuracy = 0.0;
        double newAccuracy;
        double missRate;
        double output;
        double input;
        double target;
        double[] outputLabel = new double[DIMENSION];
        int epochCount = -1;
        int numAttributes = features.cols();
        int numInstances = features.rows();
        int insignificantEpochs = 0;

        // initialize weights
        this.weights = new double[numAttributes + 1];
        for(int w = 0; w < this.weights.length; w++) weights[w] = INITIAL_WEIGHT;

        if(verbose) {
            System.out.println();
            System.out.println("**Perceptron Data**");
            System.out.println("Learning Rate:");
            System.out.println("\t" + LEARNING_RATE);
            System.out.println("Insignificant Ephocs Max:");
            System.out.println("\t" + INSIGNIFICANT_EPOCHS_MAX);
            System.out.println("Significant Accuracy:");
            System.out.println("\t" + ACCURACY_CHANGE_MIN * 100 + "%");
            System.out.println("(Ephocs, Misclassification)");
        }

        while(!done) {

            // start an epoch
            epochCount++;
            features.shuffle(this.rand, labels);

            // iterate on instances
            for(int j = 0; j < numInstances; j++) {

                // predict
                this.predict(features.row(j), outputLabel);
                output = outputLabel[0];
                target = labels.get(j, 0);

                // if necessary apply learning rule
                if(output != target) {
                    for(int k = 0; k < numAttributes; k++) {
                        input = features.get(j, k);
                        this.weights[k] += perceptronLearningRule(LEARNING_RATE, target, output, input);
                    }
                    input = BIAS_INPUT;
                    this.weights[numAttributes] += perceptronLearningRule(LEARNING_RATE, target, output, input);
                }
            }

            // check the accuracy, see if they all met the minimum
            newAccuracy = this.measureAccuracy(features, labels, null);
            if(Math.abs(accuracy - newAccuracy) < ACCURACY_CHANGE_MIN) {
                insignificantEpochs++;
                if(insignificantEpochs > INSIGNIFICANT_EPOCHS_MAX) {
                    done = true;
                }
            }
            else {
                insignificantEpochs = 0;
                accuracy = newAccuracy;
            }

            // print ephocs vs misclassification rate
            if(verbose) {
                missRate = 1.0 - this.measureAccuracy(features, labels, null);
                System.out.print("\t" + epochCount + ", " + missRate + "\n");
            }
        }

        if(verbose) {
            System.out.println("Number of Epochs: \n\t" + epochCount);
            System.out.println("Weights:");
            for(int i = 0; i < weights.length; i++) {
                System.out.println("\t" + i + ") " + weights[i]);
            }
            System.out.println("**End Perceptron Data**");
            System.out.println();
        }
    }

    @Override
    public void predict(double[] features, double[] labels) throws Exception {
        double netValue = 0.0;
        double output;
        int numAttributes = features.length;

        // calculate net value
        for (int i = 0; i < numAttributes; i++) {
            netValue += (features[i] * this.weights[i]);
        }
        netValue += (BIAS_INPUT * this.weights[numAttributes]);

        // compare to threshold
        if (netValue > THRESHOLD) output = YES;
        else output = NO;

        labels[0] = output;
    }

    private double perceptronLearningRule(double c, double t, double z, double x) {
        return c * (t - z) * x;
    }
}
