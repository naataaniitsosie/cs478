import java.util.*;

public class NearestNeighbor extends SupervisedLearner {

    private static final double UNKNOWN_VALUE = 1.7976931348623157E308;

    /**
     * k, the number of neighbors
     */
    private int k = 15;

    /**
     * weighted values of nodes
     */
    private static final Boolean WEIGHTED = true;

    /**
     * condensed nearest neighbor algorithm
     */
    private static final Boolean CNN = true;

    /**
     * a flag indicating regression or classification values
     */
    private Boolean regression;

    /**
     * the distance weight
     */
    private Double distanceWeight;

    /**
     * The neighbor features, initialized during training
     */
    private Matrix nnFeatures;

    /**
     * neighbor labels, initialized during training
     */
    private Matrix nnLabels;

    /**
     * random number
     */
    Random rand;

    public NearestNeighbor(boolean verbose, Random rand) {
        super(verbose);
        this.regression = null;
        this.nnFeatures = null;
        this.distanceWeight = null;
        this.distanceWeight = null;
        this.rand = rand;
    }

    @Override
    public void train(Matrix features, Matrix labels) throws Exception {
        Matrix subSetFeatures;
        Matrix subSetLabels;
        double[] label;
        double[] instance;
        double[] output = new double[1];
        int numReduced = 0;

        // initialize neighbors
        this.nnFeatures = features;
        this.nnLabels = labels;

        // if continuous
        if(labels.valueCount(0) == 0) {
            regression = true;
        }
        // else nominal
        else {
            regression = false;
        }

        // condensed nearest neighbor algorithm
        if(CNN) {
            subSetFeatures = new Matrix(this.nnFeatures, 0, 0, 0, features.cols());
            subSetLabels = new Matrix(this.nnLabels, 0, 0, 0, labels.cols());
            this.nnFeatures.shuffle(rand, this.nnLabels);

            // initialize subset, one from each output class
            for(int i = 0; i < features.rows(); i++) {

                // if the subset doesn't have label, add it!
                instance = this.nnFeatures.row(i);
                label = this.nnLabels.row(i);
                if(!subSetLabels.hasInstance(label)) {
                    subSetFeatures.m_data.add(instance);
                    subSetLabels.m_data.add(label);
                    features.m_data.remove(i);
                    labels.m_data.remove(i);

                    // already have one of each output class
                    if(subSetLabels.rows() >= this.nnLabels.valueCount(0)) {
                        i = features.rows();
                    }
                }
            }
            this.nnFeatures = subSetFeatures;
            this.nnLabels = subSetLabels;

            // classify each instance in the test set
            for(int j = 0; j < features.rows(); j++) {
                instance = features.row(j);
                label = labels.row(j);

                //if(nnFeatures.hasInstance(instance)) continue;

                this.predict(instance, output);

                // if not predicted correctly, add to subset
                if(output[0] != label[0]) {
                    this.nnFeatures.m_data.add(instance);
                    this.nnLabels.m_data.add(label);
                }
                else {
                    numReduced++;
                }
            }

            System.out.println("Condensed Nearest Neighbor Rule: " + numReduced + " Instanced Removed");
        }
    }

    @Override
    public void predict(double[] features, double[] labels) throws Exception {

        // variables
        double output;
        int oldK = this.k;

        // if less than k neighbors
        if(this.nnFeatures.rows() < this.k) {
            this.k = nnFeatures.rows();
        }

        // k nearest neighbors, this is the bulk of the problem
        Tuple[] neighbors = this.calculateNeighbors(features);

        // calculate the distance weight
        this.distanceWeight = this.sumDistanceWeight(neighbors);

        // regression or classification
        if(regression) {
            output = nnRegression(neighbors);
        }
        else {
            output = nnClassification(neighbors);
        }

        // set output
        labels[0] = output;

        // reset
        this.distanceWeight = null;
        this.k = oldK;
    }

    private double nnRegression(Tuple[] neighbors) {
        double output = 0.0;
        double v;
        Tuple t;
        int length = neighbors.length;

        if(WEIGHTED) {
            for(int i = 0; i < length; i++) {
                t = neighbors[i];
                v = nnLabels.get(t.index, 0) * (1 / Math.pow(t.distance, 2));
                output += v;
            }
            output /= this.distanceWeight;
        }
        else {
            for(int i = 0; i < length; i++) {
                t = neighbors[i];
                output += nnLabels.get(t.index, 0);
            }
            output /= this.k;
        }

        return output;
    }

    private double nnClassification(Tuple[] neighbors) {
        double output = 0.0;
        double distance;
        double[] classVotes = new double[nnLabels.valueCount(0)];
        Tuple t;
        int index;
        int length = neighbors.length;
        int maxIndex;

        if(WEIGHTED) {
            for(int i = 0; i < length; i++) {
                index = neighbors[i].index;
                distance = neighbors[i].distance;
                classVotes[(int) nnLabels.get(index, 0)] += 1 / Math.pow(distance, 2);
            }
            maxIndex = 0;
            for(int j = 0; j < classVotes.length; j++) {
                classVotes[j] /= this.distanceWeight;
                if(classVotes[maxIndex] < classVotes[j]) {
                    maxIndex = j;
                }
            }
            output = maxIndex;
        }
        else {
            for(int i = 0; i < length; i++) {
                index = neighbors[i].index;
                classVotes[(int) nnLabels.get(index, 0)]++;
            }
            maxIndex = 0;
            for(int j = 0; j < classVotes.length; j++) {
                classVotes[j] /= length;
                if(classVotes[maxIndex] < classVotes[j]) {
                    maxIndex = j;
                }
            }
            output = maxIndex;
        }
        return output;
    }

    private double sumDistanceWeight(Tuple[] neighbors) {
        double distanceWeight = 0.0;
        for (int i = 0; i < neighbors.length; i++) {
            distanceWeight += 1 / Math.pow(neighbors[i].distance, 2);
        }
        return distanceWeight;
    }

    private Tuple[] calculateNeighbors(double[] features) {
        Tuple[] nearest = new Tuple[this.k];
        int numInstances = this.nnFeatures.rows();
        Iterator<Tuple> iter;
        TreeSet<Tuple> neighbors = new TreeSet<Tuple>();
        Tuple t;
        int j;

        // distances
        for(int i = 0; i < numInstances; i++) {
            t = new Tuple(i, this.distance(features, i));
            neighbors.add(t);
            if(neighbors.size() > k) {
                neighbors.remove(neighbors.last());
            }
        }

        // find nearest neighbors
        iter = neighbors.iterator();
        j = 0;
        while(iter.hasNext()) {
            nearest[j] = iter.next();
            j++;
        }

        return nearest;
    }

    private double distance(double[] features, int row) {
        // Manhattan distance
        double distance = 0.0;
        double nnFeature;
        double feature;
        for (int c = 0; c < this.nnFeatures.cols(); c++) {
            nnFeature = this.nnFeatures.get(row, c);
            feature = features[c];

            // if continuous
            if(this.nnFeatures.valueCount(c) == 0) {
                if(nnFeature == UNKNOWN_VALUE) {
                    nnFeature = this.nnFeatures.columnMean(c);
                }
                if(feature == UNKNOWN_VALUE) {
                    feature = this.nnFeatures.columnMean(c);
                }
                distance += Math.abs(feature - nnFeature);
            }

            // else nominal
            else {
                if(nnFeature == UNKNOWN_VALUE || feature == UNKNOWN_VALUE) {
                    distance += 1;
                }
                else if(features[c] != nnFeature) {
                    distance += 1;
                }
                else {
                    distance += 0;
                }
            }
        }
        return distance;
    }

    private double euclidianDistance(double[] features, int row) {
        double distance = 0.0;
        for (int c = 0; c < this.nnFeatures.cols(); c++) {
            distance += Math.pow(this.nnFeatures.get(row, c) - features[c], 2);
        }
        distance = Math.sqrt(distance);
        return distance;
    }

    private class Tuple implements Comparable {

        public int index;
        public double distance;
        public Tuple(int x, double y) {
            this.index = x;
            this.distance = y;
        }

        @Override
        public int compareTo(Object o) {
            if (this.distance > ((Tuple) o).distance) {
                return 1;
            } else if (this.distance == ((Tuple) o).distance) {
                return 0;
            }
            else {
                return -1;
            }
        }

        @Override
        public boolean equals(Object o) {
            return this.index == ((Tuple) o).index && this.distance == ((Tuple) o).distance;
        }
    }
}
