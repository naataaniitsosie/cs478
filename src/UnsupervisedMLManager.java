import java.util.Random;

public class UnsupervisedMLManager {

    /**
     * normalize the data
     */
    boolean normalize = true;

    /**
     * shuffle the data
     */
    boolean shuffle = true;

    /**
     * verbose data output
     */
    boolean verbose = true;

    public void run(String[] args) throws Exception {
        UnsupervisedLearner usLearner;
        Random rand = new Random();
        int numIgnored = args.length - 1;
        String[] attributes = new String[numIgnored];

        // load data
        Matrix features = new Matrix();
        features.loadArff(args[0]);

        // preprocess data
        if(this.normalize) {
            features.normalize(null);
        }
        if(this.shuffle) {
            features.shuffle(rand);
        }
        if(args.length > 1) {
            System.arraycopy(args, 1, attributes, 0, numIgnored);
            features.removeAttributes(attributes);
        }

        // train
        usLearner = new KMeansCluster(this.verbose, features);
        usLearner.learn();

    }

    public static void main(String[] args) throws Exception
    {
        UnsupervisedMLManager ml = new UnsupervisedMLManager();
        ml.run(args);
    }
}


